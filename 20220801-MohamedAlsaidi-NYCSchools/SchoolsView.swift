//
//  SchoolsView.swift
//  20220801-MohamedAlsaidi-NYCSchools
//
//  Created by Mohamed Alsaidi on 8/1/22.
//

import SwiftUI

class HighSchoolsVM: ObservableObject {
    @Published var schools: [School]?
    
    private let fetch = DataFetch<[School]>.HighSchools
    @Published var error: Error?
    
    
    init(){
        fetch.$value.assign(to: &$schools)
    }
    
    func load(){
        fetch.load { [weak self] error in
            guard let self = self else {return}
            if self.fetch.value == nil {
                print("error")
            }
            
        }
    }
}


struct ListViewModels: View {
    @ObservedObject var viewModel = HighSchoolsVM()
    
    var body: some View {
        if let schools = viewModel.schools {
            NavigationView{
                SchoolsList(viewModel: .init(schools: schools))
                    .navigationTitle("Schools List")
            }
            } else {
                ProgressView()
                    .onAppear {self.viewModel.load()}
            }
        }
    }

