//
//  scoreList.swift
//  20220801-MohamedAlsaidi-NYCSchools
//
//  Created by Mohamed Alsaidi on 8/1/22.
//

import SwiftUI

struct ScoresList: View {
    @ObservedObject var viewModel: ScoresViewList
    
    var body: some View {
        if let scores = viewModel.scores {
            scoresView(for: scores)
        }else {
            ProgressView()
                .onAppear{ self.viewModel.load() }
        }
    }
    
    private func scoresView(for scores: SchoolScore) -> some View {
        VStack {
            Text(viewModel.schoolName)
                .font(.title2)
                .multilineTextAlignment(.center)
            Spacer().frame (height: 20)
            VStack{
                Spacer()
                VStack{
                    Text("Math")
                    Text(scores.satMathAvgScore)
                }
                Spacer()
                VStack{
                    Text("Writing")
                    Text(scores.satWritingAvgScore)
                }
                Spacer()
                VStack{
                    Text("Reading")
                    Text(scores.satCriticalReadingAvgScore)
                }
            }
            
        }
    }
}


class ScoresViewList: ObservableObject {
    @Published var scores: SchoolScore?
    @Published var fetchs: DataFetch<[SchoolScore]>
    @Published var error: Swift.Error?
    
    
    private let school: School
    var schoolName: String {
        school.schoolName
    }
    
    init(
        school: School,
        remote: DataFetch<[SchoolScore]>
    ){
        self.school = school
        self.fetchs = remote
    }
    
    func load() {
        fetchs.load { [weak self] error in
            guard let allScores = self?.fetchs.value else {
                print("error")
                return
            }
            guard let scores = allScores.first(where: {
                $0.schoolName.lowercased() == self?.school.schoolName.lowercased()
            })else {
                print("error")
                return
            }
            self?.scores = scores
            
        }
    }
    
}
