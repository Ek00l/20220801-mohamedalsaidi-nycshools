//
//  Schools.swift
//  20220801-MohamedAlsaidi-NYCSchools
//
//  Created by Mohamed Alsaidi on 8/1/22.
//

import Foundation

struct School: Decodable, Identifiable {
    
    var schoolName: String
    var interest1: String
    var id: String {schoolName}
    
}

struct SchoolScore: Decodable {
    
     var schoolName: String
     var satCriticalReadingAvgScore: String
     var satMathAvgScore: String
     var satWritingAvgScore: String
    
}

enum SchoolsURL {
    
    case schools
    case scores
    
    var url: URL {
        switch self {
        case.schools:
            return URL("https://data.cityofnewyork.us/resource/s3k6-pzi2.json")
        case.scores:
            return URL("https://data.cityofnewyork.us/resource/f9bf-2cp4.json")
        }
    
    }
}

extension URL {
    init(_ string: StaticString) {
        self.init(string: "\(string)")!
    }
}


final class DataFetch<A>: ObservableObject {
    enum LoadError: Error {
        case unknown
        case parsing
    }
    
    let url: URL
    let parse: (Data)throws -> A
    @Published var value: A?
    
    init(
        url: URL,
        parse: @escaping (Data) throws -> A)
    {
        self.url = url
        self.parse = parse
    }
    
    func load(completion: ((LoadError?)-> Void)? = nil) {
        let main = DispatchQueue.main
        guard value == nil else {
            completion?(nil)
            return
        }
        URLSession.shared.dataTask(with: url) { data,_,_ in
            guard let data = data else {
                completion?(.unknown)
                return
            }
            do {
                let result = try self.parse(data)
                main.async {
                    self.value = result
                    completion?(nil)
                }
                
            }catch {
                completion?(.parsing)
            }
        }.resume()
    }
}

extension DataFetch where A: Decodable {
    
    convenience init (url: URL) {
        self.init(
            url: url,
            parse: {data in
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                return try decoder.decode(A.self, from: data)
            }
        )
    }
}

extension DataFetch {
    static var HighSchools: DataFetch<[School]> {
        DataFetch<[School]>(url: SchoolsURL.schools.url)
    }
    static var HSchoolScores: DataFetch<[SchoolScore]> {
        DataFetch<[SchoolScore]>(url: SchoolsURL.scores.url)
    }
}

