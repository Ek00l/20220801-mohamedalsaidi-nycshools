//
//  _0220801_MohamedAlsaidi_NYCSchoolsApp.swift
//  20220801-MohamedAlsaidi-NYCSchools
//
//  Created by Mohamed Alsaidi on 8/1/22.
//

import SwiftUI

@main
struct _0220801_MohamedAlsaidi_NYCSchoolsApp: App {
    var body: some Scene {
        WindowGroup {
            ListViewModels()
        }
    }
}
