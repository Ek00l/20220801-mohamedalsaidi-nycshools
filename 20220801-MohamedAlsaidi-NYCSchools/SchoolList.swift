//
//  SchoolList.swift
//  20220801-MohamedAlsaidi-NYCSchools
//
//  Created by Mohamed Alsaidi on 8/1/22.
//

import SwiftUI


struct SchoolsList: View {
    @ObservedObject var viewModel: ListView
    
    var body: some View {
        List{
            ForEach(viewModel.schools) { school in
                NavigationLink {
                    ScoresList(
                        viewModel: .init(
                            school: school,
                            remote: viewModel.scoresData
                        )
                    )
                } label: {
                    VStack (alignment: .leading, spacing: 5) {
                        
                        Text(school.schoolName)
                        Spacer()
                        Text(school.interest1)
                    }
                }
            }
        }
        .listStyle(.plain)
    }
}

class ListView: ObservableObject{
    @Published var schools: [School]
    let scoresData: DataFetch<[SchoolScore]> = DataFetch<[SchoolScore]>.HSchoolScores
    
    init(schools: [School]) {
        self.schools = schools
    }
}
